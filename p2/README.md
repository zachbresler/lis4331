> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advance Mobile App Development

## Zachary Bresler

### Project 2 Requirements:

*Sub-Heading:*

1. screenshots of user app
2. update,add,delete,splash 

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1.
* *Screenshot* of running application's splash screen.
* *Screenshot* of running application's add.
* *Screenshot* of running application's update
* *Screenshot* of running application's delete

#### Assignment Screenshots:

Screenshot of Splash Screen             |  Screenshot of Add       |
:-------------------------:|:-------------------------:|---------------------------
![](img/splash.png)  |  ![Main Screenshot](img/add.png)  |

Screenshot of update Screen             |  Screenshot of View         
:-------------------------:|:-------------------------:
![Playing Screenshot](img/update.png)  |  ![Pause Screenshot](img/view.png)  

*Screenshot of running JDK SS7-SS9*:

|         Screenshot of delete          |      |
| :-----------------------------------: | :--: |
| ![Playing Screenshot](img/delete.png) |      |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/zachbresler/bitbucketstationlocations/ "Bitbucket Station Locations")
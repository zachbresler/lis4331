> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advance Mobile App Development

## Zachary Bresler

### A5 Requirements:

*Sub-Heading:*

1. Splash screen image, app title, intro text.
2. Include artists' images and media.
3. Images and buttons must be vertically and horizontally aligned.
4. Must add background color(s) or theme.
5. Create and display launcher icon image.

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1.
* *Screenshot* of running application's splash screen.
* *Screenshot* of running application's follow-up screen.
* *Screenshot* of running application's play and pause user interfaces.

#### Assignment Screenshots:

Screenshot of Splash Screen             |  Screenshot of Main Screen             |
:-------------------------:|:-------------------------:|---------------------------
![SS7 Screenshot](splash.png)  | ![SS7 Screenshot](imain.png) |

Screenshot of error Screen             |  Screenshot of valid screen  
:--------------------------|:-------------------------:
![SS7 Screenshot](error.png)  |  ![SS7 Screenshot](valid.png)  

*Screenshot of running JDK SS7-SS9*:

Screenshot of SS14             |  Screenshot of SS15         | Screenshot of SS9             
:-------------------------:|:-------------------------:|:------------------------------------------------:
![SS7 Screenshot](ss14.png)  |  ![SS8 Screenshot](ss15.png)  | ![SS9 Screenshot](img/imagenamehere.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/zachbresler/bitbucketstationlocations/ "Bitbucket Station Locations")